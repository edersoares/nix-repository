<?php

namespace Nix\Repository;

/**
 * CriteriaRepositoryInterface
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
interface CriteriaRepositoryInterface
{
    /**
     * Apply a criteria to the repository.
     *
     * @param mixed $builder
     * @param \Nix\Repository\RepositoryInterface $repository
     * @return void
     */
    public function apply($builder, RepositoryInterface $repository);
}
