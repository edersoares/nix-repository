<?php

namespace Nix\Repository\Base;

use Nix\Repository\ValidatorInterface;

/**
 * Validator
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
class Validator implements ValidatorInterface
{
    /**
     * Get a validation factory instance.
     *
     * @return \Illuminate\Contracts\Validation\Factory
     */
    protected function getValidationFactory()
    {
        return app('validator');
    }

    /**
     * Returns the rules to create.
     *
     * @return array
     */
    public function rulesToCreate()
    {
        return [];
    }

    /**
     * Returns the rules to update.
     *
     * @return array
     */
    public function rulesToUpdate()
    {
        return [];
    }

    /**
     * Returns if data is valid to be used in create action.
     *
     * @param array $data
     * @return boolean
     */
    public function isValidToCreate(array $data)
    {
        $this->getValidationFactory()->make($data, $this->rulesToCreate());
    }

    /**
     * Returns if data is valid to be used in update action.
     *
     * @param array $data
     * @return boolean
     */
    public function isValidToUpdate(array $data)
    {
        $this->getValidationFactory()->make($data, $this->rulesToUpdate());
    }
}