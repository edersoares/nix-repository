<?php

namespace Nix\Repository\Base;

use Closure;
use Exception;
use Nix\Repository\CriteriaRepositoryInterface;
use Nix\Repository\RepositoryInterface;
use Nix\Repository\ScopeRepositoryInterface;
use Nix\Repository\Traits\UseCriteriaTrait;
use Nix\Repository\Traits\UseScopesTrait;
use Nix\Repository\Traits\ValidatorTrait;
use Nix\Repository\Traits\WithOrdinationTrait;
use Nix\Repository\Traits\WithPaginationTrait;

/**
 * EloquentRepository
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
abstract class EloquentRepository implements RepositoryInterface
{
    use UseCriteriaTrait, UseScopesTrait, ValidatorTrait, WithOrdinationTrait,
        WithPaginationTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model;

    /**
     * Validator name.
     *
     * @var string
     */
    protected $validator;

    /**
     * Current builder.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $builder;

    /**
     * Instance the Eloquent repository and boot the traits.
     */
    public function __construct()
    {
        $this->bootUseCriteria();
        $this->bootUseScopes();
        $this->bootWithPagination();
    }

    /**
     * Returns class instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function model()
    {
        if (empty($model = $this->model)) {
            throw new Exception('Missing model name.');
        }

        return new $model;
    }

    /**
     * Returns a query builder instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newBuilder()
    {
        $this->builder = $this->model()->newQuery();

        if ($this->useCriteria()) {
            $this->getCriteria()->each(function (CriteriaRepositoryInterface $criteria) {
                $criteria->apply($this->builder, $this);
            });
        }

        if ($this->useScopes()) {
            $this->getScopes()->each(function (ScopeRepositoryInterface $scope) {
                $scope->apply($this->builder, $this);
            });
        }

        return $this->builder;
    }

    /**
     * Returns the current builder.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function builder()
    {
        if (empty($this->builder)) {
            return $this->newBuilder();
        }

        return $this->builder;
    }

    /**
     * Returns all models that correspond the scopes, criteria and callback
     * applied.
     *
     * @param \Closure|null $callback
     * @return mixed
     */
    public function get(Closure $callback = null)
    {
        $builder = $this->builder();

        if ($callback) {
            $callback($builder);
        }

        if ($this->pagination && $this->paginationSimple) {
            return $this->getWithSimplePagination($builder->get());
        }

        if ($this->pagination) {
            return $this->getWithPagination($builder->get(), $this->total());
        }

        return $builder->get();
    }

    /**
     * @inheritdoc
     */
    public function count(Closure $callback = null)
    {
        $builder = $this->builder();

        // FIXME to change to other approach
        $builder->limit(1);
        $builder->offset(0);

        if ($callback) {
            $callback($builder);
        }

        return $builder->count();
    }

    /**
     * @inheritdoc
     */
    public function all(Closure $callback = null)
    {
        $builder = $this->builder();

        if ($callback) {
            $callback($builder);
        }

        return $builder->get();
    }

    /**
     * @inheritdoc
     */
    public function create(array $data, Closure $callback = null)
    {
        if ($this->hasValidator()) {
            $this->validator()->isValidToCreate($data);
        }

        return $this->model()->create($data);
    }

    /**
     * @inheritdoc
     */
    public function read($id, Closure $callback = null)
    {
        $builder = $this->builder();
        $primary = $builder->getModel()->getKeyName();

        $builder->where($primary, $id);

        if ($callback) {
            $callback($builder);
        }

        return $builder->firstOrFail();
    }

    /**
     * @inheritdoc
     */
    public function update($id, array $data, Closure $callback = null)
    {
        if ($this->hasValidator()) {
            $this->validator()->isValidToUpdate($data);
        }

        return $this->read($id, $callback)->fill($data)->saveOrFail();
    }

    /**
     * @inheritdoc
     */
    public function delete($id, Closure $callback = null)
    {
        return $this->read($id, $callback)->delete();
    }

    /**
     * Total models.
     *
     * @param \Closure|null $callback
     * @return int
     */
    public function total(Closure $callback = null)
    {
        return $this->count($callback);
    }
}
