<?php

namespace Nix\Repository\Criteria;

use Nix\Repository\CriteriaRepositoryInterface;
use Nix\Repository\RepositoryInterface;

/**
 * PaginationCriteria
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
class PaginationCriteria implements CriteriaRepositoryInterface
{
    /**
     * The page that is returned.
     *
     * @var int
     */
    protected $page;

    /**
     * Number of the results to show.
     *
     * @var int
     */
    protected $show;

    /**
     * Instance the pagination criteria.
     *
     * @param int $page
     * @param int $show
     */
    public function __construct($page, $show)
    {
        $page = (int) $page;
        $show = (int) $show;

        if ($page < 1) {
            $page = 1;
        }

        if ($show < 1) {
            $show = 1;
        }

        $this->page = $page;
        $this->show = $show;
    }

    /**
     * @inheritdoc
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model->limit($this->show);
        $model->offset($this->show * $this->page - $this->show);
    }
}
