<?php

namespace Nix\Repository\Criteria;

use Illuminate\Http\Request;
use Nix\Repository\CriteriaRepositoryInterface;

/**
 * OrdinationByRequestCriteria
 *
 * @author Eder Soares <edersoares@me.com>
 */
class OrdinationByRequestCriteria extends OrdinationCriteria implements CriteriaRepositoryInterface
{
    /**
     * Instance the ordination criteria using a request.
     *
     * @param \Illuminate\Http\Request $request
     * @param array $allowed
     */
    public function __construct(Request $request, array $allowed)
    {
        $sort = $request->query(config('repository.query.sort'));
        $desc = $request->query(config('repository.query.desc'));

        parent::__construct($allowed, $sort, $desc);
    }
}
