<?php

namespace Nix\Repository\Criteria;

use Nix\Repository\CriteriaRepositoryInterface;
use Nix\Repository\RepositoryInterface;

/**
 * OrdinationCriteria
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
class OrdinationCriteria implements CriteriaRepositoryInterface
{
    /**
     * Allowed columns to sort.
     *
     * @var array
     */
    protected $allowed;

    /**
     * Column name to sort.
     *
     * @var string
     */
    protected $sort;

    /**
     * If desc or asc the results.
     *
     * @var string
     */
    protected $desc;

    /**
     * Instance the ordination criteria.
     *
     * @param array $allowed
     * @param string $sort
     * @param bool $desc
     */
    public function __construct(array $allowed, $sort, $desc = false)
    {
        $this->allowed = $allowed;
        $this->sort = $sort;
        $this->desc = $desc ? 'desc' : 'asc';
    }

    /**
     * @inheritdoc
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (in_array($this->sort, $this->allowed)) {
            $model->orderBy($this->sort, $this->desc);
        }
    }
}
