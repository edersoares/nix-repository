<?php

namespace Nix\Repository\Criteria;

use Illuminate\Http\Request;
use Nix\Repository\CriteriaRepositoryInterface;

/**
 * PaginationByRequestCriteria
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
class PaginationByRequestCriteria extends PaginationCriteria implements CriteriaRepositoryInterface
{
    /**
     * Instance the pagination criteria using a request.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $page = $request->query(config('repository.query.page'), 1);
        $show = $request->query(config('repository.query.show'), 10);

        parent::__construct($show, $page);
    }
}
