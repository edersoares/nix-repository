<?php

namespace Nix\Repository\Traits;

/**
 * ValidatorTrait
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
trait ValidatorTrait
{
    /**
     * Validator name.
     *
     * @var string
     */
    protected $validator;

    /**
     * Returns if validator name exists.
     *
     * @return bool
     */
    public function hasValidator()
    {
        return ! empty($this->validator);
    }

    /**
     * Returns the instance of the validator.
     *
     * @return mixed
     */
    public function validator()
    {
        $validator = $this->validator;

        return new $validator;
    }
}