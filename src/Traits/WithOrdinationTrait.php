<?php

namespace Nix\Repository\Traits;

use Nix\Repository\Criteria\OrdinationCriteria;

/**
 * WithOrdinationTrait
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
trait WithOrdinationTrait
{
    /**
     * Actives the ordination.
     *
     * @param array $allowed
     * @param string $sort
     * @param string $desc
     * @return $this
     */
    public function ordination(array $allowed, $sort, $desc)
    {
        $this->addCriteria(new OrdinationCriteria($allowed, $sort, $desc));

        return $this;
    }
}
