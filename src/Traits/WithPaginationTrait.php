<?php

namespace Nix\Repository\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Nix\Repository\Criteria\PaginationCriteria;

/**
 * WithPaginationTrait
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
trait WithPaginationTrait
{
    /**
     * Indicates if pagination is active.
     *
     * @var bool
     */
    protected $pagination;

    /**
     * @var bool
     */
    protected $paginationSimple;

    /**
     * The name of the paginator.
     *
     * @var string
     */
    protected $paginationPaginator;

    /**
     * The page that is returned.
     *
     * @var int
     */
    protected $paginationPage;

    /**
     * Number of the results to show.
     *
     * @var int
     */
    protected $paginationShow;

    /**
     * Paginator options.
     *
     * @var array
     */
    protected $paginationOptions;

    /**
     * Boot the pagination trait.
     *
     * @return void
     */
    protected function bootWithPagination()
    {
        $this->pagination = false;
        $this->paginationSimple = false;
        $this->paginationPage = 1;
        $this->paginationShow = 15;
        $this->paginationOptions = [];
    }

    /**
     * Returns a LengthAwarePaginator instance.
     *
     * @param \Illuminate\Support\Collection|array $items
     * @param int $total
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    protected function getWithPagination($items, $total)
    {
        $paginator = $this->getPaginator();

        return new $paginator(
            $items, $total, $this->paginationShow, $this->paginationPage, $this->paginationOptions
        );
    }

    /**
     * Returns a Paginator instance.
     *
     * @param \Illuminate\Support\Collection|array $items
     * @return \Illuminate\Pagination\Paginator
     */
    protected function getWithSimplePagination($items)
    {
        $paginator = $this->getPaginator();

        return new $paginator(
            $items, $this->paginationShow, $this->paginationPage, $this->paginationOptions
        );
    }

    /**
     * Returns the name of the paginator.
     *
     * @return string
     */
    public function getPaginator()
    {
        if ($this->paginationPaginator) {
            return $this->paginationPaginator;
        }

        if ($this->paginationSimple) {
            return Paginator::class;
        }

        return LengthAwarePaginator::class;
    }

    /**
     * Set the name of the paginator.
     *
     * @param $paginator
     * @return void
     */
    public function setPaginator($paginator)
    {
        $this->paginationPaginator = $paginator;
    }

    /**
     * Active the simple pagination.
     *
     * @param int $page
     * @param int $show
     * @param array $options
     * @return $this
     */
    public function simplePaginate($page, $show, array $options = [])
    {
        $this->paginate($page, $show, $options, true);

        return $this;
    }

    /**
     * Active the pagination with length.
     *
     * @param int $page
     * @param int $show
     * @param array $options
     * @param bool $simple
     * @return $this
     */
    public function paginate($page, $show, array $options = [], $simple = false)
    {
        $this->addCriteria(new PaginationCriteria($page, $show));

        $this->pagination = true;
        $this->paginationSimple = $simple;
        $this->paginationPage = (int) $page;
        $this->paginationShow = (int) $show;
        $this->paginationOptions = $options;

        return $this;
    }
}
