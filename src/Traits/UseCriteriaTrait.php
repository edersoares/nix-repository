<?php

namespace Nix\Repository\Traits;

use Illuminate\Support\Collection;
use Nix\Repository\CriteriaRepositoryInterface;

/**
 * UseCriteriaTrait
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
trait UseCriteriaTrait
{
    /**
     * All criterias.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $criteria;

    /**
     * Indicates whether criteria will be used.
     *
     * @var bool
     */
    protected $useCriteria;

    /**
     * Boot the criteria trait.
     *
     * @return $this
     */
    protected function bootUseCriteria()
    {
        $this->clearCriteria();
        $this->withCriteria();

        return $this;
    }

    /**
     * Add a criteria.
     *
     * @param  \Nix\Repository\CriteriaRepositoryInterface $criteria
     * @return $this
     */
    public function criteria(CriteriaRepositoryInterface $criteria)
    {
        return $this->addCriteria($criteria);
    }

    /**
     * Add a criteria.
     *
     * @param  \Nix\Repository\CriteriaRepositoryInterface $criteria
     * @return $this
     */
    public function addCriteria(CriteriaRepositoryInterface $criteria)
    {
        $this->criteria->push($criteria);

        return $this;
    }

    /**
     * Clear all criteria.
     *
     * @return $this
     */
    public function clearCriteria()
    {
        $this->criteria = new Collection();

        return $this;
    }

    /**
     * Returns all criteria.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Returns whether criteria will be used.
     *
     * @return bool
     */
    public function useCriteria()
    {
        return $this->useCriteria;
    }

    /**
     * Use criteria.
     *
     * @return $this
     */
    public function withCriteria()
    {
        $this->useCriteria = true;

        return $this;
    }

    /**
     * Don't use criteria.
     *
     * @return $this
     */
    public function withoutCriteria()
    {
        $this->useCriteria = false;

        return $this;
    }
}
