<?php

namespace Nix\Repository\Traits;

use Illuminate\Support\Collection;
use Nix\Repository\ScopeRepositoryInterface;

/**
 * UseScopesTrait
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
trait UseScopesTrait
{
    /**
     * All scopes.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $scopes;

    /**
     * Indicates whether scopes will be used.
     *
     * @var bool
     */
    protected $useScopes;

    /**
     * Boot the scopes trait.
     *
     * @return $this
     */
    protected function bootUseScopes()
    {
        $this->clearScopes();
        $this->withScopes();

        return $this;
    }

    /**
     * Add a scope.
     *
     * @param \Nix\Repository\ScopeRepositoryInterface $scope
     * @return $this
     */
    public function scope(ScopeRepositoryInterface $scope)
    {
        return $this->addScope($scope);
    }

    /**
     * Add a scope.
     *
     * @param \Nix\Repository\ScopeRepositoryInterface $scope
     * @return $this
     */
    public function addScope(ScopeRepositoryInterface $scope)
    {
        $this->scopes->push($scope);

        return $this;
    }

    /**
     * Clear all scopes.
     *
     * @return $this
     */
    public function clearScopes()
    {
        $this->scopes = new Collection();

        return $this;
    }

    /**
     * Returns all scopes.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * Returns whether scopes will be used.
     *
     * @return bool
     */
    public function useScopes()
    {
        return $this->useScopes;
    }

    /**
     * Use scope.
     *
     * @return $this
     */
    public function withScopes()
    {
        $this->useScopes = true;

        return $this;
    }

    /**
     * Don't use scope.
     *
     * @return $this
     */
    public function withoutScopes()
    {
        $this->useScopes = false;

        return $this;
    }
}
