<?php

namespace Nix\Repository\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * RepositoryServiceProvider
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/repository.php', 'repository');
    }
}
