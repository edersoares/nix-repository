<?php

namespace Nix\Repository;

use Closure;

/**
 * RepositoryInterface
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
interface RepositoryInterface
{
    /**
     * Count models.
     *
     * @param \Closure|null $callback
     * @return int
     */
    public function count(Closure $callback = null);

    /**
     * All models.
     *
     * @param \Closure|null $callback
     * @return mixed
     */
    public function all(Closure $callback = null);

    /**
     * Create a model.
     *
     * @param array $data
     * @param \Closure|null $callback
     * @return mixed
     */
    public function create(array $data, Closure $callback = null);

    /**
     * Read a model.
     *
     * @param int $id
     * @param \Closure|null $callback
     * @return mixed
     */
    public function read($id, Closure $callback = null);

    /**
     * Update a model.
     *
     * @param int $id
     * @param array $data
     * @param \Closure|null $callback
     * @return mixed
     */
    public function update($id, array $data, Closure $callback = null);

    /**
     * Delete a model.
     *
     * @param int $id
     * @param \Closure|null $callback
     * @return mixed
     */
    public function delete($id, Closure $callback = null);
}
