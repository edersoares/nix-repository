<?php

namespace Nix\Repository;

/**
 * ValidatorInterface
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
interface ValidatorInterface
{
    /**
     * Returns the rules to create.
     *
     * @return array
     */
    public function rulesToCreate();

    /**
     * Returns the rules to update.
     *
     * @return array
     */
    public function rulesToUpdate();

    /**
     * Returns if data is valid to be used in create action.
     *
     * @param array $data
     * @return boolean
     */
    public function isValidToCreate(array $data);

    /**
     * Returns if data is valid to be used in update action.
     *
     * @param array $data
     * @return boolean
     */
    public function isValidToUpdate(array $data);
}