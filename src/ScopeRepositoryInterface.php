<?php

namespace Nix\Repository;

/**
 * ScopeRepositoryInterface
 *
 * @author Eder Soares <edersoares@me.com>
 * @package Nix\Repository
 */
interface ScopeRepositoryInterface
{
    /**
     * Apply a criteria to the repository.
     *
     * @param mixed $builder
     * @param \Nix\Repository\RepositoryInterface $repository
     * @return void
     */
    public function apply($builder, RepositoryInterface $repository);
}
