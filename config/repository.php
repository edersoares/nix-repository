<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Query String
    |--------------------------------------------------------------------------
    |
    | This is the default configuration to the names used in request's query
    | string and are passed to PaginationByRequestCriteria and
    | OrdinationByRequestCriteria.
    |
    | sort: the column name that is used to sort results.
    | page: which page is returned
    | show: number of results is shown
    | desc: show results descending or ascending
    |
    */
    'query' => [
        'sort' => 'sort',
        'page' => 'page',
        'show' => 'show',
        'desc' => 'desc'
    ]
];
